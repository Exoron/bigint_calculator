#pragma once

#include <string_view>
#include <optional>
#include "bigint.hpp"

std::optional<char> FindFirstSignificantChar(std::string_view& input);

const BigInteger ReadBigint(std::string_view& input);