#pragma once

#include <iostream>
#include <vector>
#include <concepts>

class BigInteger {
public:
    BigInteger() = default;

    template<std::integral T>
    explicit BigInteger(T number);

    BigInteger& operator+=(const BigInteger&);
    friend const BigInteger operator+(const BigInteger& a,
                                             const BigInteger& b);
    BigInteger& operator-=(const BigInteger&);
    friend const BigInteger operator-(const BigInteger& a,
                                             const BigInteger& b);
    BigInteger& operator*=(const BigInteger&);
    friend const BigInteger operator*(const BigInteger& a,
                                             const BigInteger& b);
    BigInteger& operator/=(const BigInteger&);
    friend const BigInteger operator/(const BigInteger& a,
                                             const BigInteger& b);
    BigInteger& operator%=(const BigInteger&);
    friend const BigInteger operator%(const BigInteger& a,
                                             const BigInteger& b);

    const BigInteger operator+() const;
    const BigInteger operator-() const;
    BigInteger& operator++();
    const BigInteger operator++(int);
    BigInteger& operator--();
    const BigInteger operator--(int);

    bool operator<(const BigInteger&) const;
    bool operator<=(const BigInteger&) const;
    bool operator>(const BigInteger&) const;
    bool operator>=(const BigInteger&) const;
    bool operator==(const BigInteger&) const;
    bool operator!=(const BigInteger&) const;

    int operator[](int i) const;

    friend std::istream& operator>>(std::istream&, BigInteger&);

    std::string ToString() const;
    int Size() const;
    bool IsNegative() const;
    bool IsZero() const;

    explicit operator bool() const;

private:
    void AbsPlus(const BigInteger& number);
    void AbsPositiveMinus(const BigInteger& number);
    void AbsNegativeMinus(const BigInteger& number);

    void DigitMultiply(int digit);
    void ShiftLeft(size_t n);
    void ShiftRight(size_t n);
    void RemoveLeadingZeros();

    bool AbsLess(const BigInteger& number) const;

private:
    std::vector<int> digits;
    bool minus = false;
};

std::ostream& operator<<(std::ostream& out, const BigInteger& number);