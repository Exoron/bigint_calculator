#include "bigint.hpp"

template<std::integral T>
BigInteger::BigInteger(T number) {
    if (number == 0) {
        return;
    }
    if (number < 0) {
        // avoid overflow from minusing T_MIN
        digits.push_back(-(number % 10));
        number /= 10;

        minus = true;
        number *= -1;
    }
    while (number > 0) {
        digits.push_back(number % 10);
        number /= 10;
    }
    RemoveLeadingZeros();
}

BigInteger& BigInteger::operator+=(const BigInteger& number) {

    if (IsNegative() == number.IsNegative()) {
        if(this != &number) {
            AbsPlus(number);
        } else {
            BigInteger copy = number;
            AbsPlus(copy);
        }
        return *this;
    }
    if (AbsLess(number)) {
        minus = !minus;
        AbsNegativeMinus(number);
    } else {
        AbsPositiveMinus(number);
    }
    return *this;
}

const BigInteger operator+(const BigInteger& a, const BigInteger& b) {
    BigInteger result = a;
    result += b;
    return result;
}

BigInteger& BigInteger::operator-=(const BigInteger& number) {
    if (IsNegative() != number.IsNegative()) {
        if(this != &number) {
            AbsPlus(number);
        } else {
            BigInteger copy = number;
            AbsPlus(copy);
        }
        return *this;
    }
    if (AbsLess(number)) {
        AbsNegativeMinus(number);
        minus = !minus;
    } else {
        AbsPositiveMinus(number);
    }
    return *this;
}

const BigInteger operator-(const BigInteger& a, const BigInteger& b) {
    BigInteger result = a;
    result -= b;
    return result;
}

BigInteger& BigInteger::operator*=(const BigInteger& number) {
    bool sign = (IsNegative() != number.IsNegative());

    BigInteger res(0);
    minus = false; // to work with abs value
    for(int i = number.Size() - 1; i >= 0; --i) {
        BigInteger additive = *this;
        additive.DigitMultiply(number[i]);
        additive.ShiftLeft(i);
        res += additive;
    }
    *this = std::move(res);
    minus = sign;
    return *this;
}

const BigInteger operator*(const BigInteger& a, const BigInteger& b) {
    BigInteger result = a;
    result *= b;
    return result;
}

void BigInteger::ShiftLeft(size_t n) { digits.insert(digits.begin(), n, 0); }

void BigInteger::ShiftRight(size_t n) {
    digits.erase(digits.begin(), digits.begin() + n);
}

void BigInteger::DigitMultiply(int digit) {
    for (int i = 0; i < Size(); ++i) {
        digits[i] *= digit;
    }
    int max_positive_digit = -1;
    for (int i = 0; i < Size(); ++i) {
        if (digits[i] > 9) {
            if (i + 1 == Size()) {
                digits.push_back(digits[i] / 10);
            } else {
                digits[i + 1] += digits[i] / 10;
            }
            digits[i] %= 10;
        }
        if (digits[i] > 0) {
            max_positive_digit = i;
        }
    }
    digits.resize(max_positive_digit + 1);
}

BigInteger& BigInteger::operator/=(const BigInteger& number) {
    if(number == BigInteger{0}) {
        throw std::runtime_error("div by zero");
    }
    if (Size() < number.Size()) {
        *this = BigInteger{0};
        return *this;
    }

    bool sign = (IsNegative() != number.IsNegative());
    minus = false;
    BigInteger copy = number;
    copy.minus = false;
    int diff = Size() - copy.Size();
    std::vector<int> result(diff + 1, 0);
    copy.ShiftLeft(diff);

    for (int i = 0; i <= diff; ++i) {
        while (copy <= *this) {
            *this -= copy;
            ++result[diff - i];
        }
        copy.ShiftRight(1);
    }
    digits = result;
    minus = sign;
    RemoveLeadingZeros();
    return *this;
}

const BigInteger operator/(const BigInteger& a, const BigInteger& b) {
    BigInteger result = a;
    result /= b;
    return result;
}

BigInteger& BigInteger::operator%=(const BigInteger& number) {
    *this -= (*this / number) * number;
    return *this;
}

const BigInteger operator%(const BigInteger& a, const BigInteger& b) {
    BigInteger result = a;
    result %= b;
    return result;
}

const BigInteger BigInteger::operator+() const {
    return *this;
}

const BigInteger BigInteger::operator-() const {
    BigInteger result = *this;
    result.minus = !result.minus;
    return result;
}

BigInteger& BigInteger::operator++() { return *this += BigInteger{1}; }

const BigInteger BigInteger::operator++(int) {
    BigInteger copy = *this;
    ++*this;
    return copy;
}

BigInteger& BigInteger::operator--() { return *this -= BigInteger{1}; }

const BigInteger BigInteger::operator--(int) {
    BigInteger copy = *this;
    --*this;
    return copy;
}

bool BigInteger::operator<(const BigInteger& number) const {
    if(IsZero() && number.IsZero()) {
        return false;
    }
    if (IsNegative() && number.IsNegative()) {
        return number.AbsLess(*this);
    }
    if (IsNegative() && !number.IsNegative()) {
        return true;
    }
    if (!IsNegative() && number.IsNegative()) {
        return false;
    }
    return AbsLess(number);
}

bool BigInteger::operator<=(const BigInteger& number) const {
    return !(number < *this);
}

bool BigInteger::operator>(const BigInteger& number) const {
    return number < *this;
}

bool BigInteger::operator>=(const BigInteger& number) const {
    return !(*this < number);
}

bool BigInteger::operator==(const BigInteger& number) const {
    return !(*this < number || number < *this);
}

bool BigInteger::operator!=(const BigInteger& number) const {
    return !(*this == number);
}

int BigInteger::operator[](int i) const { return digits[i]; }

std::istream& operator>>(std::istream& in, BigInteger& number) {
    number.digits.resize(0);
    number.minus = false;
    std::string number_string;
    in >> number_string;
    int n = number_string.length();
    if (n == 0) {
        number.digits.resize(0);
        return in;
    }
    for(int i = 0; i < n; ++i) {
        if(!isdigit(number_string[i]) && (number_string[i] != '-' || i != 0)) {
            number_string.erase(number_string.begin() + i, number_string.end());
            n = number_string.length();
            break;
        }
    }
    int string_index = n - 1;
    if (number_string[0] == '-') {
        number.minus = true;
        --n;
    }
    for (int i = 0; i < n; ++i) {
        number.digits.push_back(number_string[string_index] - '0');
        --string_index;
    }
    number.RemoveLeadingZeros();
    return in;
}

std::ostream& operator<<(std::ostream& out, const BigInteger& number) {
    if (number.IsZero()) {
        out << 0;
        return out;
    }
    if (number.IsNegative()) {
        out << "-";
    }
    for (int i = number.Size() - 1; i >= 0; --i) {
        out << number[i];
    }
    return out;
}

std::string BigInteger::ToString() const {
    if (Size() == 0) {
        return "0";
    }
    std::string answer;
    if (IsNegative()) {
        answer.push_back('-');
    }
    for (int i = Size() - 1; i >= 0; --i) {
        answer.push_back(digits[i] + '0');
    }
    return answer;
}

int BigInteger::Size() const { return digits.size(); }

BigInteger::operator bool() const { return operator!=(BigInteger(0)); }

void BigInteger::AbsPlus(const BigInteger& number) {
    for (int i = 0; i < Size() || i < number.Size(); ++i) {
        if (i >= Size()) {
            digits.push_back(number[i]);
            continue;
        } else if(i < number.Size()) {
            digits[i] += number[i];
        }
        if (digits[i] < 10) {
            continue;
        }
        if (i + 1 == Size()) {
            digits.push_back(1);
        } else {
            digits[i + 1] += 1;
        }
        digits[i] -= 10;
    }
    RemoveLeadingZeros();
}

void BigInteger::AbsPositiveMinus(const BigInteger& number) {
    for (int i = 0; i < Size(); ++i) {
        if (i < number.Size()) {
            digits[i] -= number[i];
        }
        if (digits[i] < 0) {
            digits[i + 1] -= 1;
            digits[i] += 10;
        }

        RemoveLeadingZeros();
    }
}

void BigInteger::AbsNegativeMinus(const BigInteger& number) {
    for (int i = 0; i < number.Size(); ++i) {
        if (i >= Size()) {
            digits.push_back(number[i]);
            continue;
        }
        digits[i] -= number[i];

        if (digits[i] > 0) {
            if (i + 1 == Size()) {
                digits.push_back(1);
            } else {
                digits[i + 1] += 1;
            }
            digits[i] -= 10;
        }
        digits[i] *= -1;
    }

    RemoveLeadingZeros();
}

bool BigInteger::AbsLess(const BigInteger& number) const {
    if (Size() != number.Size()) {
        return Size() < number.Size();
    }
    for (int i = Size() - 1; i >= 0; --i) {
        if (digits[i] != number[i]) {
            return digits[i] < number[i];
        }
    }
    return false;
}

bool BigInteger::IsNegative() const { return minus; }

void BigInteger::RemoveLeadingZeros() {
    int i = digits.size() - 1;
    for (; i >= 0; --i) {
        if (digits[i] != 0) {
            break;
        }
    }
    digits.resize(i + 1);
}

bool BigInteger::IsZero() const {
    return (Size() == 0);
}