#pragma once

#include <string_view>
#include <memory>
#include "bigint.hpp"

class ASTNode {
public:
    virtual const BigInteger Compute() const = 0;
};

using NodePtr = std::unique_ptr<ASTNode>;

class NumberNode: public ASTNode {
public:
    explicit NumberNode(BigInteger x);
    const BigInteger Compute() const override;

private:
    const BigInteger number_;
};

class UnaryOperation: public ASTNode {
public:
    explicit UnaryOperation(NodePtr expr);

protected:
    const BigInteger GetValue() const;

private:
    NodePtr expr_;
};

class UnaryPlus: public UnaryOperation {
public:
    explicit UnaryPlus(NodePtr expr);
    const BigInteger Compute() const override;
};

class UnaryMinus: public UnaryOperation {
public:
    explicit UnaryMinus(NodePtr expr);
    const BigInteger Compute() const override;
};

class BinaryOperation: public ASTNode {
public:
    BinaryOperation(NodePtr lhs, NodePtr rhs);

    const BigInteger GetLhs() const;
    const BigInteger GetRhs() const;
private:
    NodePtr lhs_;
    NodePtr rhs_;
};

class BinaryPlus: public BinaryOperation {
public:
    BinaryPlus(NodePtr lhs, NodePtr rhs);

    const BigInteger Compute() const override;
};

class BinaryMinus: public BinaryOperation {
public:
    BinaryMinus(NodePtr lhs, NodePtr rhs);

    const BigInteger Compute() const override;
};

class BinaryMult: public BinaryOperation {
public:
    BinaryMult(NodePtr lhs, NodePtr rhs);

    const BigInteger Compute() const override;
};

class BinaryDiv: public BinaryOperation {
public:
    BinaryDiv(NodePtr lhs, NodePtr rhs);

    const BigInteger Compute() const override;
};

class BinaryMod: public BinaryOperation {
public:
    BinaryMod(NodePtr lhs, NodePtr rhs);

    const BigInteger Compute() const override;
};

class AST {
public:
    AST(std::string_view input);

    const BigInteger Compute() const;
private:
    static NodePtr Parse(std::string_view& input);
    static NodePtr ParseOperation(std::string_view& input);
    static NodePtr ParseOperand(std::string_view& input);
    static std::pair<NodePtr, char> ParseHighPriorityBatch(std::string_view& input);

private:
    NodePtr root_;
};
