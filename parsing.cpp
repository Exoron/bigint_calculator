#include "parsing.hpp"
#include <sstream>

std::optional<char> FindFirstSignificantChar(std::string_view& input) {
    auto pos = input.find_first_not_of(' ');
    if(pos == std::string_view::npos) {
        return std::nullopt;
    }
    input.remove_prefix(pos);
    return input.front();
}

const BigInteger ReadBigint(std::string_view& input) {
    FindFirstSignificantChar(input);
    auto pos = input.find_first_not_of("0123456789");

    BigInteger number;
    std::stringstream(input.substr(0, pos).data()) >> number;

    if(pos == std::string_view::npos) {
        input.remove_prefix(input.length());
    } else {
        input.remove_prefix(pos);
    }

    return number;
}