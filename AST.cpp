#include "AST.hpp"
#include "parsing.hpp"

NumberNode::NumberNode(BigInteger x): number_(std::move(x)) {}

const BigInteger NumberNode::Compute() const {
    return number_;
}

UnaryOperation::UnaryOperation(NodePtr expr): expr_(std::move(expr)) {}

const BigInteger UnaryOperation::GetValue() const {
    return expr_->Compute();
}

UnaryPlus::UnaryPlus(NodePtr expr): UnaryOperation(move(expr)) {}

const BigInteger UnaryPlus::Compute() const {
    return GetValue();
}

UnaryMinus::UnaryMinus(NodePtr expr): UnaryOperation(move(expr)) {}

const BigInteger UnaryMinus::Compute() const {
    return -GetValue();
}

BinaryOperation::BinaryOperation(NodePtr lhs, NodePtr rhs): lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}

const BigInteger BinaryOperation::GetLhs() const {
    return lhs_->Compute();
}

const BigInteger BinaryOperation::GetRhs() const {
    return rhs_->Compute();
}

BinaryPlus::BinaryPlus(NodePtr lhs, NodePtr rhs) : BinaryOperation(std::move(lhs), std::move(rhs)) {}

const BigInteger BinaryPlus::Compute() const {
    return GetLhs() + GetRhs();
}

BinaryMinus::BinaryMinus(NodePtr lhs, NodePtr rhs) : BinaryOperation(std::move(lhs), std::move(rhs)) {}

const BigInteger BinaryMinus::Compute() const {
    return GetLhs() - GetRhs();
}

BinaryMult::BinaryMult(NodePtr lhs, NodePtr rhs) : BinaryOperation(std::move(lhs), std::move(rhs)) {}

const BigInteger BinaryMult::Compute() const {
    return GetLhs() * GetRhs();
}

BinaryDiv::BinaryDiv(NodePtr lhs, NodePtr rhs) : BinaryOperation(std::move(lhs), std::move(rhs)) {}

const BigInteger BinaryDiv::Compute() const {
    return GetLhs() / GetRhs();
}

BinaryMod::BinaryMod(NodePtr lhs, NodePtr rhs) : BinaryOperation(std::move(lhs), std::move(rhs)) {}

const BigInteger BinaryMod::Compute() const {
    return GetLhs() % GetRhs();
}


AST::AST(std::string_view input) {
    root_ = Parse(input);
}

const BigInteger AST::Compute() const {
    return root_->Compute();
}

NodePtr AST::Parse(std::string_view& input) {
    auto front = FindFirstSignificantChar(input);
    if(!front) {
        return {};
    }

    if(std::isdigit(*front) || front == '+' || front == '-') {
        return ParseOperation(input);
    }

    if(FindFirstSignificantChar(input) == '(') {
        input.remove_prefix(1); // remove (
        auto expr = Parse(input); // either more braces or ParseOperation
        input.remove_prefix(1); // remove )
        return expr;
    }
    return {};
}

NodePtr ApplyOp(NodePtr lhs, NodePtr rhs, char op) {
    if(!lhs) {
        return rhs;
    }
    switch(op) {
        case '+':
            return std::make_unique<BinaryPlus>(std::move(lhs), std::move(rhs));
        case '-':
            return std::make_unique<BinaryMinus>(std::move(lhs), std::move(rhs));
        case '*':
            return std::make_unique<BinaryMult>(std::move(lhs), std::move(rhs));
        case '/':
            return std::make_unique<BinaryDiv>(std::move(lhs), std::move(rhs));
        case '%':
            return std::make_unique<BinaryMod>(std::move(lhs), std::move(rhs));
        default:
            return {};
    }
}

NodePtr ApplyUnaryOp(NodePtr expr, char unary_op) {
    switch(unary_op) {
        case '+':
            return std::make_unique<UnaryPlus>(std::move(expr));
        case '-':
            return std::make_unique<UnaryMinus>(std::move(expr));
        default:
            return expr;
    }
}

NodePtr AST::ParseOperand(std::string_view& input) {
    char front = input.front();
    char unary_op = '\0';
    NodePtr operand;

    if (front == '+' || front == '-') {
        unary_op = front;
        input.remove_prefix(1);
    }
    if (auto next = FindFirstSignificantChar(input); next && *next == '(') {
        // doesn't happen unless unary op exist
        // if happens, process as a full expression what's inside the brackets
        operand = Parse(input);
    } else {
        // operand of a unary op or just a number
        operand = std::make_unique<NumberNode>(ReadBigint(input));
    }
    return ApplyUnaryOp(std::move(operand), unary_op);
}

std::pair<NodePtr, char> AST::ParseHighPriorityBatch(std::string_view& input) {
    // accumulates the result of a single high-priority batch
    NodePtr lhs;

    // for priority ops
    char op = '\0';

    // high-priority operation batch
    while (true) {
        // one operand of high-priority op
        NodePtr rhs = ParseOperand(input);

        // apply priority op to previous operands
        lhs = ApplyOp(std::move(lhs), std::move(rhs), op);

        // last high-priority batch
        // operation will not be used
        auto front = FindFirstSignificantChar(input);
        if (!front || *front == ')') {
            return {std::move(lhs), '\0'};
        }

        op = *front;
        input.remove_prefix(1);

        // low priority operator
        // start processing next high-priority batch
        if (op == '+' || op == '-') {
            return {std::move(lhs), op};
        }
        // else continue process priority operations
        // we want to apply them in straight order until they end
    }
}

NodePtr AST::ParseOperation(std::string_view &input) {
    char curr_op = '\0';

    // accumulates the results of low-priority ops applied to high-priority batches
    NodePtr lhs;

    while(FindFirstSignificantChar(input) && input.front() != ')') {
        auto&& [rhs, next_op] = ParseHighPriorityBatch(input);

        lhs = ApplyOp(std::move(lhs), std::move(rhs), curr_op);

        // low-priority op
        // apply to next batch
        curr_op = next_op;
    }

    return lhs;
}
