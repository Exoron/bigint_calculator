#include <iostream>
#include "AST.hpp"

int main() {
    std::string input;
    std::cout << "Type your expression: " << std::endl;
    std::getline(std::cin, input);

    AST ast(input);

    std::cout << "Your result:" << std::endl << ast.Compute() << std::endl;

    return 0;
}
